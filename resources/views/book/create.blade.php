@extends('layouts.app')

@section('content')

    <div class="row d-flex justify-content-center">
        <div class="ml-3">
            <a href="{{ route('book.index') }}"  class="btn btn-secondary mr-2 text-white">Back</a>
        </div>
        <h2 class="text-center ml-3 mb-3">Create New Book</h2>
        
    </div>
    
    <div class="row justify-content-center mt-1">
        <div class="col-md-8">
            <form method="POST" action="{{ route('book.store') }}">
                @csrf
                <input id="status" name="status" type="hidden" value="available">

                <div class="form-group">
                    <label for="name"> Title </label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Name of the book" />
                </div>

                <div class="form-group">
                    <label for="author"> Author </label>
                    <input type="text" name="author" class="form-control" id="author" placeholder="Name of the author" />
                </div>

                <div class="form-group">
                    <label for="published_date"> published date </label>
                    <input type="date" name="published_date" class="form-control" id="published_date" placeholder=" " />
                </div>

                <div class="form-group">
                    <label for="category_id"> Category </label>
                    <input onkeyup="fetchC()" type="text" name="category_id" class="form-control" id="category_id" placeholder="category" />
                    <div id= "category_list"></div>
                </div>

                <div class="form-group d-flex justify-content-center">
                    <input type="submit" class="btn btn-primary" value="Add Book">
                </div>
            </form>
        </div>
    </div>
@endsection

<script>
    function fetchC()
    {
        var query = document.getElementById("category_id").value;
        data = {"_token": "{{ csrf_token() }}", "query" : query};
        if(query != '')
        {
            $.ajax({
                url:"{{route('category.fetch') }}",
                method: "POST",
                data : data,
                success:function(data){
                    $('#category_list').fadeIn();
                    $('#category_list').html(data);
                }
            })
        }
        $(document).on('click', 'li', function(){  
            $('#category_id').val($(this).text());  
            $('#category_list').fadeOut();  
        })
    }

</script>