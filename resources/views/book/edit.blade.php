@extends('layouts.app')

@section('buttons')
    <a href="{{ route('book.index') }}"  class="btn btn-secondary mr-2 text-white">Back</a>
@endsection

@section('content')

<h2 class="text-center mb-3">Edit Book : {{ $book_edit->name}}</h2>
    
    <div class="row justify-content-center mt-1">
        <div class="col-md-8">
        <form method="POST" action="{{ route('book.update', ['book' => $book_edit->id]) }}">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name"> Title </label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Name of the book" value="{{ $book_edit->name}}" />
                </div>

                <div class="form-group">
                    <label for="author"> Author </label>
                    <input type="text" name="author" class="form-control" id="author" placeholder="Name of the author" value="{{ $book_edit->author}}" />
                </div>

                <div class="form-group">
                    <label for="published_date"> published date </label>
                    <input type="date" name="published_date" class="form-control" id="published_date" placeholder="yyyy-mm-dd" value="{{ $book_edit->published_date->format('Y-m-d')}}"/>
                </div>

                <div class="form-group">
                    <label for="category_id"> Category </label>
                    <input type="integer" name="category_id" class="form-control" id="category_id" placeholder="category" value="{{ $book_edit->category->name}}"/>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save Change">
                </div>
            </form>
        </div>
    </div>
@endsection