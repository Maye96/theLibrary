@extends('layouts.app')


@section('content')
    <div class="row d-flex justify-content-center">
        <h2 class="text-center mb-1">Books</h2>
        <div class="ml-3">
            <a href="{{ route('book.create') }}"  class="btn btn-secondary mr-2 text-white">Add a Book </a>
           
        </div>

    </div>

    <div class="col-md-10 mx-auto bg-white p-3">
        <form class="form-inline p-2 float-right">
            <input name = "searchBook" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" value="{{ $searchBook }}">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
        <table class="table" id="book-table">
            <thead class="bg-primary text-light">
                <tr>
                    <th class="d-none">Id</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Author</th>
                    <th>Category</th>
                    <th>Published date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($books as $book)
                <tr>
                    <td class="d-none" id="row-id-{{$book->id}}"></td>
                    <td id="book-status-{{$book->id}}"> {{ $book->status }}</td>
                    <td> {{ $book->name }}</td>
                    <td> {{ $book->author }}</td>
                    <td> {{ $book->category}}</td>
                    <td> {{ $book->published_date }}</td>
                    <td>
                        <a href="{{ route('book.edit' ,  ['book' => $book->id]) }}" class="btn btn-success mr-1">Edit</a> 
                        <a href="javascript:void(0)" onclick="removeModal({{ $book->id }})" class="btn btn-danger mr-1">Delete</a> 
                        <a href="javascript:void(0)" onclick="showModalA({{ $book->id }})" class="btn btn-dark" id="available-book" data-toggle="modal" > Available </a>
                        <a href="javascript:void(0)" onclick="lendModalB({{ $book->id }}, '{{ $book->name }}')" class="btn btn-primary">Lend book</a>
                    </td>   
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="d-flex justify-content-center py-4 mb-1">
        {{ $books->links() }}
    </div>


         <!-- Modal Available -->
        <div class="modal fade" id="avalaibleModal" tabindex="-1" role="dialog" aria-labelledby="avalaibleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header bg-primary text-light">
                  <h5 class="modal-title" id="avalaibleModalLabel"><strong> Book status </strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="status" id="status1" value="available">
                      <label class="form-check-label" for="status1"> Available </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="status" id="status2" value="not available">
                      <label class="form-check-label" for="status2"> Not available </label>
                    </div>
                    
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="saveAvalaible">Save changes</button>
                </div>
              </div>
            </div>
        </div>

        <!-- Modal Delete-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-light">
                        <h5 class="modal-title" id="deleteModalLabel"><strong> Delete a book</strong></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body d-flex justify-content-center float-md-left">
                        <div>
                            <img src="images/alert.jpg" width="300" height="200">
                            <h3 class="text-center"> Are you sure ? </h3>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> cancel </button>
                        <button type="button" class="btn btn-primary" id="deleteBook"> ok </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Lend book -->
        <div class="modal fade" id="lendBookModal" tabindex="-1" aria-labelledby="lendBookModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-light">
                    <h5 class="modal-title" id="lendBookModalLabel"> <strong> Lend book </strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form id="formLend">
                            @csrf
                            
                            <div class="form-group">
                                <label for="book-title">Book title</label>
                                <input type="text" name="book-tittle" class="form-control" id="book-title" placeholder="Name that borrowed" required disabled/>
                            </div>

                            <div class="form-group">
                                <label for="user_thatborrowed"> Name that borrowed </label>
                                <input type="text" name="user_thatborrowed" class="form-control" id="user_thatborrowed" placeholder="Name that borrowed" required />
                            </div>

                            <div class="form-group">
                                <label for="borrowed_date"> Borrowed date </label>
                                <input type="date" name="borrowed_date" class="form-control" id="borrowed_date" placeholder=" " required/>
                            </div>

                            <div class="form-group">
                                <label for="returned_date"> Returned date (opcional) </label>
                                <input type="date" name="returned_date" class="form-control" id="returned_date" placeholder=" " />
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary" type= "button" id= "saveBorrowed">Save changes</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
          
          


@endsection

<script>

    function showModalA(data)
    {
        
        $("#avalaibleModal").modal("show");
        document.getElementById('saveAvalaible').onclick = function(e){
            data = {"_token": "{{ csrf_token() }}","id" : data, "status" : $("input[name=status]:checked").val()};
            
            $.ajax({
                type : "POST",
                url : "{{route('book.status')}}",
                data : data,
                success : function(response){
                    $("#avalaibleModal").modal("hide");
                    document.getElementById('book-status-'+data["id"]).innerHTML = response["status"];
                    swal( response["success"]);
                },
                error : function(response){
                    swal(response.responseJSON["success"]);
                    $("#avalaibleModal").modal("hide");
                }
            });
        }
    }

    function removeModal(data)
    {
        $("#deleteModal").modal("show");
        document.getElementById('deleteBook').onclick = function(e){
            id = data;
            data =  {"_token": "{{ csrf_token() }}"};
            $.ajax({
                type : "DELETE",
                url : "/book/" + id,
                data:data,
                success : function(e){
                    $("#deleteModal").modal("hide");
                    row = document.getElementById("row-id-"+id);
                    document.getElementById("book-table").deleteRow(row.parentNode.rowIndex);
                    location.reload();
                },
                error : function(e){
                    swal("There's a problem deleted the book");
                    $("#deleteModal").modal("hide");
                }
            });

        }

    }

    function lendModalB(id, name)
    {
        $("#lendBookModal").modal("show");
        document.getElementById("book-title").value = name;
        document.getElementById('saveBorrowed').onclick = function(e)
        {
            data = {
                'book_id': id,
                'user_thatborrowed' : document.getElementById('user_thatborrowed').value,
                'borrowed_date' : document.getElementById('borrowed_date').value,
                'returned_date' : document.getElementById('returned_date').value,
                "_token": "{{ csrf_token() }}",
            };
            
            $.ajax({
                type : "POST",
                url : "{{route('borrowing.store')}}",
                data:data,
                success : function(response){
                    $("#lendBookModal").modal("hide");
                    document.getElementById('book-status-'+data["book_id"]).innerHTML = response["status"];
                    swal(response["success"]);
                },
                error : function(response){
                    swal(response.responseJSON["success"]);
                    $("#lendBookModal").modal("hide");
                }
            });
            console.log(data);
        }
    }

</script>