@extends('layouts.app')

@section('buttons')
    
@endsection

@section('content')
    <h2 class="text-center mb-1">Who borrowed</h2>

    <div class="col-md-10 mx-auto bg-white p-3">
        <form class="form-inline p-2 float-right">
            <input name = "searchBorrowed" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" value="{{ $searchBorrowed }}">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
        <table class="table">
            <thead class="bg-primary text-light">
                <tr>
                    <th>Book</th>
                    <th>Person thatborrowed a book</th>
                    <th>Borrowed date</th>
                    <th>Returned date</th>
                    <th class="bg-warning">User returned date</th>
                    <th class="bg-warning">Observation</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($borrowing as $item)
                    <tr>
                        <td>{{ $item->book_name }}</td>
                        <td>{{ $item->user_thatborrowed}} </td>
                        <td>{{ $item->borrowed_date}} </td>
                        <td>{{ $item->returned_date}}</td>
                        <td id="user_returned_date_{{$item->id}}">{{ $item->user_returned_date}}</td>
                        <td id="observations_{{$item->id}}">{{ $item->observation}}</td>
                        <td>                            
                            <a href="javascript:void(0)" onclick="returnedModalB({{ $item->id }})" class="btn btn-success">return</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="d-flex justify-content-center py-4 mb-1">
        {{ $borrowing->links() }}
    </div>

    <!-- Modal returned -->
    <div class="modal fade" id="returnedModal" tabindex="-1" aria-labelledby="returnedModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-light">
                    <h5 class="modal-title" id="returnedModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="needs-validation" novalidate id="formReturned">

                        <div class="form-group">
                            <label for="user_returned_date"> User returned date </label>
                            <input type="date" name="user_returned_date" class="form-control" id="user_returned_date" placeholder=" " required/>
                            <div class="invalid-feedback">
                                Please provide a date.
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="observation"> Observation </label>
                            <input type="text" name="observation" class="form-control" id="observation" placeholder=" " required />
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id = "saveReturned">Save changes</button>
                        </div>
                    </form>
                </div>
               
            </div>
        </div>
    </div>
@endsection

<script>

    function returnedModalB(id)
    {
        
        $("#returnedModal").modal("show");
        document.getElementById('saveReturned').onclick = function(e)
        {
            data = {
                'id': id,
                'user_returned_date' : document.getElementById('user_returned_date').value,
                'observation' : document.getElementById('observation').value,
                "_token": "{{ csrf_token() }}",
            };
            console.log(data);
            $.ajax({
                type : "POST",
                url : "{{route('borrowing.storeReturned')}}",
                data:data,
                success : function(response){
                    $("#returnedModal").modal("hide");
                    swal(response["success"]);
                    document.getElementById('user_returned_date_'+data["id"]).innerHTML = response["user_returned_date"]
                    document.getElementById('observations_'+data["id"]).innerHTML = response["observation"]
                },
                error : function(response){
                    swal(response.responseJSON["success"]);
                    $("#returnedModal").modal("hide");
                }
            });
        }
       
    }

</script>


