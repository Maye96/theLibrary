<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeletedToBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            //
            Schema::table('books', function (Blueprint $table) {
                //
                if (!Schema::hasColumn('books', 'deleted'))
                {
                    Schema::table('books', function (Blueprint $table) {
                        //
                        $table->enum('deleted', ['yes', 'not'])->default('not');
                    });
                }
                
            });
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            //
            if (Schema::hasColumn('books', 'deleted'))
            {
                Schema::table('books', function (Blueprint $table) {
                    //
                    $table->dropColumn(['deleted' ]);
                });
            }
        });
    }
}
