<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToBorrowings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('borrowings', function (Blueprint $table) {
            //
            if (!Schema::hasColumn('borrowings', 'user_returned_date'))
            {
                $table->date('user_returned_date')->nullable();
            }
            if (!Schema::hasColumn('borrowings', 'observation'))
            {
                $table->longText('observation')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('borrowings', function (Blueprint $table) {
            //
            if (Schema::hasColumn('borrowings', 'user_returned_date'))
            {   
                $table->dropColumn(['user_returned_date' ]);
            }
            if (Schema::hasColumn('borrowings', 'observation'))
            {   
                $table->dropColumn(['observation' ]);
            }
        });
    }
}
