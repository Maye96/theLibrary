<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('books')->insert([
            'category_id' => '5',
            'name' => 'The Turner Diaries',
            'author' => ' William Luther Pierce',
            'published_date' => '1978-10-28',
        ]);

        DB::table('books')->insert([
            'category_id' => '1',
            'name' => 'Island',
            'author' => '	Aldous Huxley',
            'published_date' => '1883-11-14',
        ]);

        DB::table('books')->insert([
            'category_id' => '2',
            'name' => 'The love and the live',
            'author' => 'Omaira Suarez',
            'published_date' => '2020-10-20',
        ]);

        DB::table('books')->insert([
            'category_id' => '1',
            'name' => 'Don Quixote ',
            'author' => 'Miguel de Cervantes',
            'published_date' => '1620-10-10',
        ]);

        DB::table('books')->insert([
            'category_id' => '1',
            'name' => 'One Hundred Years of Solitude ',
            'author' => 'Gabriel Garcia Marquez',
            'published_date' => '1620-10-10',
        ]);
    }
}

