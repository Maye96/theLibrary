<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            'name' => 'Fiction',
            'description' => 'contain a made-up story – a story that did not actually happen in real life. ',
        ]);

        DB::table('categories')->insert([
            'name' => 'Action and Adventure',
            'description' => 'The stories under this genre usually show an event or a series of events that happen outside the course of the protagonist’s ordinary life. ',
        ]);

        DB::table('categories')->insert([
            'name' => 'Classic',
            'description' => 'Classic refers to the fictions that are accepted as the most important and influential books of a particular time period or place.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Crime and Detective',
            'description' => 'As the name suggests, this book genre deals with crime, criminal motives and the investigation and detection of the crime and criminals.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Drama',
            'description' => 'Dramas are stories composed in verse or prose, usually for theatrical performance, where conflicts and emotions are expressed through dialogue and actions.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Horror',
            'description' => 'Horror is a genre that is intended to or has the ability to create the feeling of fear, repulsion, fright or terror in the readers. In other words, it creates a frightening and horror atmosphere.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Humor',
            'description' => 'Humor fiction is usually full of fun, fancy, and excitement. It is meant to entertain and sometimes cause intended laughter in readers.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Cook Books',
            'description' => 'Recipes and the history of food. These cooking books can also be in a narrative form, where the author gives context to the importance of the recipe.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Recreation',
            'description' => 'These books cover hobbies that are mostly done for enjoyment.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Pet Care',
            'description' => 'Books about looking after pets and other animals.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Communications',
            'description' => 'Books that relate to the ways that humans communicate, including foreign languages.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Home, Garden, and DIY',
            'description' => 'Books about the house, the garden, and how to maintain a beautiful garden.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Reference',
            'description' => 'Necessary and objective information, like dictionaries, encyclopedias, book abstracts and study guides, and books of quotations.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Business',
            'description' => 'Business books are all around the creation and management of a company.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Psychology',
            'description' => 'Books that examine mental and emotional functions and well-being.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Social Science',
            'description' => 'Social Science books look at social relationships and relationships. This includes anthropology, sociology, political science, and law books.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Science',
            'description' => 'Science books talk about physical sciences like mathematics, technology, chemistry, biology, physics, engineering, and more.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Philosophy',
            'description' => 'An academic view of knowledge and existence.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Mythology',
            'description' => 'Books or stories about ancient religions that are not actively practiced anymore.',
        ]);
        
       
        DB::table('categories')->insert([
            'name' => 'Religion',
            'description' => 'Books that explore a particular faith.
            These religious texts can include history, books about the practice, or holy books.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Music',
            'description' => 'Music books include books that talk about the history of music, music theory, or songbooks.',
        ]);

        DB::table('categories')->insert([
            'name' => 'Art',
            'description' => 'Books that talk about or collect art. This can be visual art like paintings, or physical arts like theatre.',
        ]);
        
        DB::table('categories')->insert([
            'name' => 'Poetry',
            'description' => 'Writing where the author (called a ‘poet’) gives emphasis to rhythm and style. Poetry is usually written in verse.',
        ]);

        DB::table('categories')->insert([
            'name' => 'History',
            'description' => 'Books that look at past events. Some history books are usually heavily researched and cover either a specific country or time.',
        ]);

    }
}
