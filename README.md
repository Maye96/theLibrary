# Library Project

## Requirements

* Laravel 7.0
* Postgresql 12.4

## Steps to run the project

* git clone https://gitlab.com/Maye96/theLibrary.git
* cd theLibrary
* composer update
* cp .env.example .env
* nano.env (Inside .env change the parameters with DB_*, example:)
    ```
    DB_CONNECTION=pgsql
    DB_HOST=127.0.0.1
    DB_PORT=5432
    DB_DATABASE=laravel
    DB_USERNAME=root
    DB_PASSWORD=
    ```
* php artisan key:generate
* Create inside the database the db with name DB_DATABASE (If you working in console you can do:)
    ```
    createdb DB_DATABASE -U DB_USERNAME -h DB_HOST
    ```
* php artisan migrate
* php artisan db:seed

## To run the server use

* php artisan serve

## To enter the system use this user:

* User -> admin@library.com
* Password -> secret