<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'id',
        'name',
        'description'
    ];

 
    protected $casts = [

        'name'=> 'string',
        'description'=> 'string',
    ];

    
    ///Un Libro tiene muchas categorias
    function books(){
        return $this->hasMany(Book::class, 'category_id', 'id');
    }
}
