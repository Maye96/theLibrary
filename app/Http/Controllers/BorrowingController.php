<?php

namespace App\Http\Controllers;

use App\Book;
use App\Borrowing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class BorrowingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $searchBorrowed = $request->get('searchBorrowed');

        if(is_null($searchBorrowed)){
            $borrowing = DB::table('borrowings')->join('books', 'borrowings.book_id', '=', 'books.id')
            ->orderBy('borrowings.id', 'ASC')
            ->select('borrowings.id',
                    'books.name AS book_name' ,
                    'borrowings.user_thatborrowed', 
                    'borrowings.borrowed_date',
                    'borrowings.returned_date',
                    'borrowings.user_returned_date', 
                    'borrowings.observation')
            ->paginate(5);

        }else {
            $borrowing = DB::table('borrowings')->join('books', 'borrowings.book_id', '=', 'books.id')
            ->Where('books.name', 'ilike', "%{$searchBorrowed}%")
            ->orwhere('borrowings.user_thatborrowed', 'ilike', "%{$searchBorrowed}%")
            ->orWhere('borrowings.borrowed_date', 'ilike', "%{$searchBorrowed}%")
            ->orWhere('borrowings.returned_date', 'ilike', "%{$searchBorrowed}%")
            ->orWhere('borrowings.user_returned_date', 'ilike', "%{$searchBorrowed}%")
            ->orWhere('borrowings.observation', 'ilike', "%{$searchBorrowed}%")
            ->orderBy('borrowings.id', 'ASC')
            ->select('borrowings.id',
                    'books.name AS book_name' ,
                    'borrowings.user_thatborrowed', 
                    'borrowings.borrowed_date',
                    'borrowings.returned_date',
                    'borrowings.user_returned_date', 
                    'borrowings.observation')
            ->paginate(5);
        }
        

        return view('borrowing.index', compact('borrowing', 'searchBorrowed'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $book = Book::find($request->get('book_id'));
    
        if($book->status == 'available')
        {
            $book->status =  'not available';
            $borrowing = Borrowing::create($request->all());
            $borrowing->save();
            $book->save();
            return response()->json(['success' => 'Borrowing updated.','status' => $book->status]);
        }else{
            
            return response()->json(['success' => 'Book not available.'])->setStatusCode(500);
        }
    }

    public function storeReturned(Request $request)
    {

        $borrowing = Borrowing::find($request->get('id'));
        $book = Book::find($borrowing->book_id);

        if(is_null($borrowing->user_returned_date)){
            $book->status =  'available';
            $book->save();
            $borrowing->user_returned_date = $request->get('user_returned_date');
            $borrowing->observation = $request->get('observation');
            $borrowing->save();
            return response()->json(['success' => 'Book returned.',
            'user_returned_date' => $request->get('user_returned_date'),
            'observation' => $request->get('observation')]);
        }else{
            return response()->json(['success' => 'The book is already returned'])->setStatusCode(500);
        }
       
    
    }

}
