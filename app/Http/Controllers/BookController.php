<?php

namespace App\Http\Controllers;

use App\Book;
use App\Borrowing;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use DataTables;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    public function index(Request $request)
    {
        //

        $searchBook = $request->get('searchBook');

        if(is_null($searchBook))
        {
            $books = DB::table('books')->join('categories', 'books.category_id', '=', 'categories.id')
            ->where('books.deleted', '=', 'not')
            ->orderBy('books.id', 'ASC')
            ->select('books.id', 
                    'books.status',
                    'books.name', 
                    'books.author',
                    'books.published_date', 
                    'categories.name AS category',
                    'books.deleted')
            ->paginate(5);
        }else{
            $books = DB::table('books')
            ->selectRaw("
            books.id as id,
            books.status,
            books.name as name ,
            books.author  as author, 
            categories.name as category,
            books.published_date as published_date,
            books.deleted as deleted")
            ->join('categories','categories.id', "=", "books.category_id")
            ->whereRaw("books.deleted = 'not' and ((lower(books.name) LIKE  ?) or (lower(books.author) LIKE ?) or (lower(books.status) LIKE ?) or (lower(categories.name) LIKE ?))", array('%'.trim(strtolower($searchBook)).'%', '%'.trim(strtolower($searchBook)).'%', '%'.trim(strtolower($searchBook)).'%', '%'.trim(strtolower($searchBook)).'%') )
            ->orderBy("books.id","ASC")
            ->paginate(5);
            
        }

        return view('book.index', compact('books', 'searchBook'));

    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $category = Category::where('name', '=', $request->get('category_id'))->get();

        $request['category_id'] = $category[0]->id;

        $book = Book::create($request->all());
        $book->save();

        return redirect()->action('BookController@index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
        $book_edit = Book::with(['category'])->find($book->id);
        return view('book.edit', compact('book_edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //

        $book_edit = Book::find($book->id);

        $category = Category::where('name', '=', $request->get('category_id'))->get();
        $request['category_id'] = $category[0]->id;

        $book_edit->fill($request->all())->save();
        return redirect()->action('BookController@index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     * 
     * Se agrego el campo deleted, para indicar que se elimino pero mantener el historial de libros prestados
     */
    public function destroy($id)
    {
        //
        $book = Book::find($id);
        $book->deleted = 'yes';
        $book->save();
    
    }

    /***
     *  Status: Status es una función que habilita o deshabilita un libro.
     *  Para poder deshabilitarlo no puede estar prestado, debe estar devuelto
     *  o en su defecto no haber sido prestado
     ***/

    public function status(Request $request)
    {
        $borrowing = Borrowing::where('id', '=', $request->get('id'))->orderBy('id', 'desc')->first();
        if(empty($borrowing)){
            $book = Book::find($request->id);
            $book->status = $request->get("status");
            $book->save();
            return response()->json([
                "success" => "Book status changed",
                "status" => $request->get("status")
            ]);
        }else{
            if(!is_null($borrowing->user_returned_date)){
                $book = Book::find($request->get('id'));
                $book->status = $request->get("status");
                $book->save();
                return response()->json([
                    "success" => "Book status changed",
                    "status" => $request->get("status")
                ]);
            }else{
                return response()->json(['success' => 'The Book is Borrowed.'])->setStatusCode(500);
            }
        }
    }
}
