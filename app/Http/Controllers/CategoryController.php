<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class CategoryController extends Controller
{
    function fetch(Request $request)
    {

        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = Category::where('name', 'ilike', "%{$query}%")->get();

            $output = '<ul class="dropdown-menu" 
                style="display:block;
                position:relative">';
            foreach($data as $row){
                $output .= '<li> <a href = "#">' .$row->name. '</a></li>';
            }
            $output .= '</ul>';
            echo $output;

        }
    }

}
