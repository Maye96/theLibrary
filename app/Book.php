<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //

    protected $fillable = [
        'id',
        'category_id',
        'name',
        'author',
        'published_date',
        'status'
    ];

 
    protected $casts = [

        'name'=> 'string',
        'author'=> 'string',
        'published_date'=> 'date:Y-m-d',
    ];

   
    ///Un Libro pertenece a una categoria
    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

}
