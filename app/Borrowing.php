<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrowing extends Model
{
    //
    protected $fillable = [
        'id',
        'book_id',
        'user_thatborrowed',
        'borrowed_date',
        'returned_date',
        'user_returned_date',
        'observation',
    ];

 
    protected $casts = [

        'user_thatborrowed'=> 'string',
        'borrowed_date'=> 'date',
        'returned_date'=> 'date',
        'user_returned_date' => 'date',
        'observation' => 'string',
    ];

   
    ///Un Libro pertenece a una categoria
    public function book() {
        return $this->belongsTo(Book::class, 'book_id', 'id');
    }
}
